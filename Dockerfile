# from https://www.drupal.org/docs/system-requirements/php-requirements
FROM php:8.0-fpm-buster as application

# install the PHP extensions we need
RUN set -eux; \
	savedAptMark="$(apt-mark showmanual)"; \
    \
	# Workaround https://unix.stackexchange.com/questions/2544/how-to-work-around-release-file-expired-problem-on-a-local-mirror
    echo "Acquire::Check-Valid-Until \"false\";\nAcquire::Check-Date \"false\";" | cat > /etc/apt/apt.conf.d/10no--check-valid-until; \
	apt-get update; \
	apt-get install --no-install-recommends --no-install-suggests -y \
		libfreetype6-dev \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
		libzip-dev \
		git \
		unzip \
	; \
	\
	docker-php-ext-configure gd \
		--with-freetype \
		--with-jpeg=/usr \
	; \
	\
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \	
	\	
    # reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
		| awk '/=>/ { print $3 }' \
		| sort -u \
		| xargs -r dpkg-query -S \
		| cut -d: -f1 \
		| sort -u \
		| xargs -rt apt-mark manual;  

WORKDIR /app

COPY composer.* load.environment.php .env ./
COPY ./web ./web
COPY ./bin ./bin

# install Drupal via comnposer
RUN set -eux; \
	php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer; \
	composer install --no-suggest; \
	composer drupal:scaffold;

# Post deplyoment
RUN ./bin/post-deploy; 

# clean up
RUN set -eux; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
# RUN { \
# 		echo 'opcache.memory_consumption=128'; \
# 		echo 'opcache.interned_strings_buffer=8'; \
# 		echo 'opcache.max_accelerated_files=4000'; \
# 		echo 'opcache.revalidate_freq=60'; \
# 		echo 'opcache.fast_shutdown=1'; \
# 	} > /usr/local/etc/php/conf.d/opcache-recommended.ini


